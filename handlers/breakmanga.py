#!/usr/bin/env python
import os

import re
import urllib2
from site_handler import SiteHandler


class BreakManga(SiteHandler):
    NAME = 'BreakManga'
    BASE_URL = 'http://www.breakmanga.com/'
    READER_URL = BASE_URL+''
    CONTENT_URL = BASE_URL[:-1]

    def __init__(self):
        SiteHandler.__init__(self)
        self.manga_url = None
        self.manga_name = None
        self.chapters_urls = {}

    def list_chapters(self, manga):
        self.manga_name = manga
        self.chapters_urls = {}
        try:
            response = urllib2.urlopen(self.READER_URL + manga.lower()+'/')
        except urllib2.HTTPError as error_http:
            if error_http.code == 404:
                return None
            raise error_http
        html = response.read()
        response.close()

        html = html[html.find('<select id="pagenumber" onchange='):]
        html = html[:html.find('</select></span></form>')]
        chapters = re.findall(r"<option value=\"([^>0-9]+([0-9]+)[^\">0-9]+)\" ", html)
        if not chapters:
            raise Exception("Can't find chapters for comic: " + manga)
        for c_url, chapter in chapters:
            self.chapters_urls[int(chapter)] = self.CONTENT_URL+''+c_url
        return self.chapters_urls.keys()

    def get_chapter_filename(self, chapter_url):
        response = urllib2.urlopen(chapter_url)
        html = response.read()
        response.close()
        file_name = re.findall(r"\[   \]\"><\/td><td><a href=\"([^\"]+)\"", html)
        if not file_name or len(file_name) != 1:
            raise Exception('No archive for chapter: %s' % chapter_url)
        return file_name[0]

    def download_chapter(self, path, chapter):
        if not chapter in self.chapters_urls.keys():
            raise Exception('Chapter unknow: %s' % str(chapter))
        chapter_url = self.chapters_urls[chapter]
        chapter_path = os.path.join(path, chapter_url.split('/')[-2])
        if not os.path.exists(chapter_path):
            os.makedirs(chapter_path)

        for i in range(1, 100):
            try:
                response = urllib2.urlopen(chapter_url+'%d#reader' % i)
            except urllib2.HTTPError as error_http:
                if error_http.code == 404:
                    break
            html = response.read()
            response.close()
            html = html[html.find('table id="picture"'):]
            html = html[:html.find('</table>')]
            file_url = html[html.find('<img src="')+10:html.find('" alt=')]
            filename = urllib2.unquote(urllib2.unquote(file_url.split('/')[-1]))
            self.download_file(chapter_path, filename, file_url, print_name=False)
        print int(chapter),
        #filename = self.get_chapter_filename(self.chapters_urls[chapter])
