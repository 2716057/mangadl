#!/usr/bin/env python

#from test import TestHandler
from wallagain import WallAgain
from comicsreader import ComicsReader
#from breakmanga import BreakManga


class SiteHandlerFactory():
    def __init__(self):
        raise Exception('This is a static class')

    @staticmethod
    def get_handlers():
        return {'': WallAgain,
                'wallagain': WallAgain,
                'comicsreader': ComicsReader,
                #'breakmanga': BreakManga,
                #'test': TestHandler,
                }

    @staticmethod
    def get_handler(name):
        handler = SiteHandlerFactory.get_handlers().get(name, None)
        if not handler:
            raise NotImplementedError("Site not supported: " + name)
        return handler

    @staticmethod
    def get_unique_handlers():
        handlers = []
        for (_, handler) in SiteHandlerFactory.get_handlers().iteritems():
            if handler not in handlers:
                handlers.append(handler)
        return handlers
