#!/usr/bin/env python
import cookielib

import os
import time
import urllib
import urllib2
#import requests
import threading


class SiteHandler():
    DEBUG = False
    NO_ARCHIVE = False
    class NoRedirection(urllib2.HTTPErrorProcessor):
        def http_response(self, request, response):
            return response
        https_response = http_response

    class NoRedirectHandler(urllib2.HTTPRedirectHandler):
        def http_error_302(self, req, fp, code, msg, headers):
            infourl = urllib.addinfourl(fp, headers, req.get_full_url())
            infourl.status = code
            infourl.code = code
            return infourl
        http_error_300 = http_error_302
        http_error_301 = http_error_302
        http_error_303 = http_error_302
        http_error_307 = http_error_302

    @classmethod
    def get_name(cls):
        return cls.NAME

    def __init__(self):
        pass

    def list_chapters(self, manga):
        raise NotImplementedError()

    def download(self, path, chapters):
        thread_pool = []
        is_ok = True
        SiteHandler.DownloadThread.init_semaphore(5)
        print '    dl: ',
        for chapter in chapters:
            thread = SiteHandler.DownloadThread(self, path, chapter)
            thread_pool.append(thread)
            thread.start()

        while len(thread_pool) > 0:
            thread = thread_pool.pop()
            while thread.isAlive():
                # Yields control to whomever is waiting
                time.sleep(0)
            if thread.is_failed:
                is_ok = False
        print '... Done.'
        return is_ok

    def download_file(self, path, filename, url, print_name=True):
        if print_name:
            print filename + ', ',
        if not self.DEBUG:
            urllib.URLopener().retrieve(url, os.path.join(path, filename))

    thread_semaphore = None

    class DownloadThread(threading.Thread):
        def __init__(self, site_handler, path, chapter):
            threading.Thread.__init__(self)
            self.site_handler = site_handler
            self.chapter = chapter
            self.path = path
            self.is_failed = False

        @staticmethod
        def init_semaphore(value):
            global thread_semaphore
            thread_semaphore = threading.Semaphore(value)

        @staticmethod
        def acquire_semaphore():
            global thread_semaphore

            if thread_semaphore is None:
                raise Exception('Semaphore not initialized')

            thread_semaphore.acquire()

        @staticmethod
        def release_semaphore():
            global thread_semaphore

            if thread_semaphore is None:
                raise Exception('Semaphore not initialized')

            thread_semaphore.release()

        def run(self):
            try:
                self.site_handler.download_chapter(self.path, self.chapter)
            except Exception as exception:
                # Assume semaphore has not been release
                # This assumption could be faulty if the error was thrown in the compression function
                # The worst case is that releasing the semaphore would allow one more thread to
                # begin downloading than it should
                #
                # If the semaphore was not released before the exception, it could cause deadlock
                thread_semaphore.release()
                self.is_failed = True
                raise Exception("Thread crashed while downloading chapter: %s" % str(exception))
