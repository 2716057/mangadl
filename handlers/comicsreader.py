#!/usr/bin/env python
import os

import re
import urllib2
import time
from site_handler import SiteHandler


class ComicsReader(SiteHandler):
    NAME = 'ComicsReader'
    BASE_URL = 'http://fr.comics-reader.com/'
    READER_URL = BASE_URL + 'series/'
    CONTENT_URL = BASE_URL + 'content/comics/'

    def __init__(self):
        SiteHandler.__init__(self)
        self.manga_url = None
        self.manga_name = None
        self.chapters_urls = {}

    def list_chapters(self, manga):
        self.manga_name = manga
        self.chapters_urls = {}
        try:
            response = urllib2.urlopen(self.READER_URL + manga.lower())
        except urllib2.HTTPError as error_http:
            if manga[-3:] != '_fr':
                return self.list_chapters(manga+'_fr')
            if error_http.code == 404:
                return None
            raise error_http
        html = response.read()
        response.close()
        self.manga_url = re.findall(r"/content/comics/([^/]+)/", html)
        if not self.manga_url:
            raise Exception("Can't find webdir for comic: " + manga)
        self.manga_url = self.manga_url[0]

        response = urllib2.urlopen(self.CONTENT_URL + self.manga_url)
        html = response.read()
        response.close()

        chapters = re.findall(r"\[DIR\]\"></td><td><a href=\"(([0-9]{2,})_[^\"]+)/", html)

        for c_url, chapter in chapters:
            self.chapters_urls[int(chapter)] = self.CONTENT_URL+self.manga_url+'/'+c_url
        return self.chapters_urls.keys()

    def get_chapter_files(self, chapter_url, no_archive):
        response = urllib2.urlopen(chapter_url)
        html = response.read()
        response.close()
        if not no_archive:
            file_names = re.findall(r"\[   \]\"></td><td><a href=\"([^\"]+)\"", html)
            if file_names and len(file_names) == 1:
                return file_names

        file_names = re.findall(r"\[IMG\]\"></td><td><a href=\"([^\"]+)\"", html)
        file_names = [img_name for img_name in file_names if img_name.find('thumb') == -1]

        if not file_names or len(file_names) < 1:
            raise Exception('No archive for chapter: %s' % chapter_url)
        return file_names

    def download_chapter(self, path, chapter):
        if not chapter in self.chapters_urls.keys():
            raise Exception('Chapter unknow: %s' % str(chapter))

        file_names = self.get_chapter_files(self.chapters_urls[chapter], self.NO_ARCHIVE)

        chapter_path = path
        print_filename = True
        chapter_url =  self.chapters_urls[chapter]
        # create subdir for files if necessary
        if len(file_names) > 1:
            print_filename = False
            chapter_path = os.path.join(path, '%03d' % chapter)
            if not os.path.exists(chapter_path):
                os.makedirs(chapter_path)

        for file in file_names:
            self.download_file(chapter_path, urllib2.unquote(file), chapter_url+'/'+file, print_name=print_filename)

        return

    def download_file(self, path, filename, url, print_name=True):
        if print_name:
            print filename + ', ',
        headers = {'Host': 'www.comics-reader.com', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36'}

        with open(os.path.join(path, filename), 'wb') as local_file:
            if not self.DEBUG:
                opener = urllib2.build_opener(SiteHandler.NoRedirectHandler())
                #urllib2.install_opener(opener)
                for i in range(3):
                    try:
                        request = urllib2.Request(url, None, headers)
                    except urllib2.URLError as e:
                        if e.code == 503:
                            time.sleep(3+i*5)
                            continue
                        else:
                            raise e
                    else:
                        break
                distant_file = opener.open(request)
                local_file.write(distant_file.read())