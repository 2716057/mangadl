#!/usr/bin/env python

from handlers.site_handler import SiteHandler

class TestHandler(SiteHandler):
    NAME='TestHandler'

    def __init__(self):
        pass

    def list_chapters(self, manga):
        return [623, 624, 625, 640, 659]

    def download_chapter(self):
        return