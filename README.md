mangadl
========================

About
-----
mangadl is a python script able to download manga chapter from a web manga reader.

Dependencies
------------

  * Python 2.7, 3.3

Tested on Linux, it should also work on OS X or Windows.

Usage
-----
To download an entire series:
(will download all available chapters that does not already exist in the target path)

    ~ $ python mangadl.py download [MANGA_NAME] [MANGA_NAME] [...] -t to_read/

To download a specific chapter:

    ~ $ python mangadl.py download [-c CHAPTER_NUMBER] [MANGA_NAME]

To download a range of manga chapter:

    ~ $ python mangadl.py download [-c RANGE_START-RANGE_END] [MANGA_NAME]

Examples
--------



