#!/usr/bin/env python

import re
import os
import argparse
from handlers.factory import SiteHandlerFactory


def args_mgt():
    parser_g = argparse.ArgumentParser(description='Manga toolkit')
    sub = parser_g.add_subparsers(title='The mangadl commands are')
    parser = sub.add_parser('interactive', help='enter interactive mode')
    parser.set_defaults(func=interactive)

    parser = sub.add_parser('list-sources', help='list available manga sources')
    parser.set_defaults(func=list_sources)

    parser = sub.add_parser('update', help='update collection described in config')
    parser.set_defaults(func=update)
    parser.add_argument('config_file', type=argparse.FileType('w', 0), metavar='file',
                        help='Update a manga collection describe in config file')

    parser = sub.add_parser('download', help='download manga chapters')
    parser.set_defaults(func=download)
    parser.add_argument('manga', nargs='+', help='Download chapter for manga(s)')
    parser.add_argument('-s', '--source', default='', help='Source to download mangas')
    parser.add_argument('-c', '--chapter', default="$-",
                        help='Define chapter to download (example: "621, 625", "620-625", "654-", "$-") ["$-"]')
    parser.add_argument('-d', '--directory', dest='target', metavar='path',
                        help='The destination directory  [current directory]')
    return parser_g


def list_sources(args):
    sources = []
    for h in SiteHandlerFactory.get_handlers().values():
        if h.get_name() not in sources:
            sources.append(h.get_name())
    print ', '.join(sources)


def update(args):
    raise NotImplementedError('Not yet implemented')


def download(args):

    if not args.target:
        args.target = os.getcwd()
    else:
        if not os.path.exists(args.target):
            os.makedirs(args.target)

    if args.source:
        sources = [SiteHandlerFactory.get_handler(args.source.lower())(), ]
    else:
        sources = [s() for s in SiteHandlerFactory.get_unique_handlers()]

    for manga in args.manga:
        print manga
        manga_path = os.path.join(args.target, manga)
        for source in sources:
            available_chapters = source.list_chapters(manga)
            if available_chapters is None:
                continue
            chapters_to_dl = define_chapter_2_dl(args.chapter, available_chapters, manga_path)

            print '  - Source: ' + source.NAME + ' -> chapters to download: '
            print '    ' + str(chapters_to_dl)

            if not os.path.exists(manga_path):
                os.mkdir(manga_path)
            source.download(manga_path, chapters_to_dl)
            break
        else:
            print '  No source found for this manga!'
    print ''
    print 'Done.'


def list_chapters(path):
    if not os.path.isdir(path):
        os.mkdir(path)
        return []
    list_dir = os.walk(path).next()[1]
    list_file = os.walk(path).next()[2]

    chapters = []
    for sub_path in list_dir+list_file:
        numbers = re.findall(r"[^0-9]*([0-9]+)[^0-9]*", sub_path)
        if not numbers:
            continue
        chapters.append(int(numbers[0]))
    return chapters


def define_chapter_2_dl(asked, available_chapters, path_to_check):
    existing_chapters = list_chapters(path_to_check)
    if not asked or asked[:2] == '$-':
        if available_chapters is None:
            return [max(existing_chapters+[0])+1, ]
        return [c for c in available_chapters if c > max(existing_chapters+[0])]
    else:
        chapters_to_dl = []
        chapters_asked = asked.replace(' ', '').split(',')
        for i in chapters_asked:
            iteration = re.search('([0-9]*)-([0-9]*)', i)
            if iteration is not None:  # it's a range
                for j in range(int(iteration.group(1)), int(iteration.group(2)) + 1):
                    if j not in existing_chapters:
                        chapters_to_dl.append(j)
            else:  # it's a single chapter
                chapters_to_dl.append(int(i))
        if available_chapters is None:
            return chapters_to_dl
        return [c for c in chapters_to_dl if c in available_chapters]


def interactive(args):
    raise NotImplementedError('Not yet implemented')


def main():
    args = args_mgt().parse_args()
    # args = args_mgt().parse_args(['download', '-c', '624', 'naruto'])
    # print "option supplied: " + str(vars(args))
    args.func(args)


if __name__ == "__main__":
    main()
